---
# try also 'default' to start simple
theme: seriph
# random image from a curated Unsplash collection by Anthony
# like them? see https://unsplash.com/collections/94734566/slidev
# background: https://source.unsplash.com/collection/94734566/1920x1080
# apply any windi css classes to the current slide
class: 'text-center'
# https://sli.dev/custom/highlighters.html
highlighter: shiki
# some information about the slides, markdown enabled
info: |
  ## Slidev Starter Template
  Presentation slides for developers.

  Learn more at [Sli.dev](https://cn.sli.dev)
---

# Network programming in Rust

Build fast , resilient and robust network servers and clients by leveraging
Rust's memory-safy and concurrency features

<!--
Rust has steadily become one of the most important new programming languages in recent
years. Like C or C++, Rust enables the developer to write code that is low-level enough to
make Rust code quickly. And since Rust is memory-safe by design, it does not allow code
that can crash on a null pointer exception. These properties make it a natural choice for
writing low-level networking applications
This title begs two questions: why should anyone care about networking? And why would anyone want to
write networking applications in Rust?
-->

---

# why should anyone care about networking

In this video , we will cover the following topics:

- **History of networking**: why and how networks came into use and how the
  internet evolved
- **Layering in networks**: how layering and encapsulation works
- **Addressing**: how networks and individual hosts are uniquely identified on
  the internet
- **How IP routing works**
- **How DNS works**
- **Service models for data delivery**
- **The network programming interface in Linux**

<style>
h1 {
  background-color: #2B90B6;
  background-image: linear-gradient(45deg, #4EC5D4 10%, #146b8c 20%);
  background-size: 100%;
  -webkit-background-clip: text;
  -moz-background-clip: text;
  -webkit-text-fill-color: transparent;
  -moz-text-fill-color: transparent;
}
</style>

<!--
We attempt to answer the first question in this video. We will introduce Rust
and network programming using Rust in next video. Firstly, in this video, we
will start with a bit of history and try to understand how network architecture
evolved over the last hundred years. In subsequent sections, we will see how
modern networks are layered and addressed. Afterwards, we will describe common
service models used in networking. We will end with a summary of
networkingrelated programming interfaces that Linux exposes. Note that this book
deliberately ignores network programming in other operating systems and focuses
only on Linux for the sake of simplicity. While the Rust compiler is
platform-agnostic, there can be cases where some things are different in other
platforms compared to Linux. We will point out those differences as we progress.
-->

---

# A brief history of networks

- **Advanced Research Projects Agency Network** (ARPANET) - 1969
- **World wide Internet** - 1973

<!--
The modern internet has revolutionized how we communicate with one another.
However, it had humble beginnings in the Victorian era. One of the earliest
precursors to the internet was telegraph networks which were operational as
early as 1850. Back then, it used to take 10 days to send a message from Europe
to North America by sea. Telegraph networks reduced that to 17 hours. By the
late 19th century, the telegraph was a fully successful communication technology
that was used widely in the two world wars. Around that time, people started
building computers to help in cracking enemy codes. Unlike our modern mobile
phones and laptops, those computing machines were often huge and needed
specialized environments to be able to operate smoothly. Thus, it was necessary
to put those in special locations while the operators would sit on a terminal.
The terminal needed to be able to communicate with the computer over short
distances. A number of local area networking technologies enabled this, the most
prominent one being Ethernet. Over time, these networks grew and by the 1960s,
some of these networks were being connected with one another to form a larger
network of networks. The Advanced Research Projects Agency Network (ARPANET) was
established in 1969 and it became the first internetwork that resembles the
modern internet. Around 1973, there were a number of such internetworks all
around the world, each using their own protocols and methods for communication.
Eventually, the protocols were standardized so that the networks could
communicate with each other seamlessly. All of these networks were later merged
to form what is the internet today. Since networks evolved in silos all around
the world, they were often organized according to geographical proximity. A
Local Area Network (LAN) is a collection of host machines in small proximity
like a building or a small neighborhood. A Wide Area Network (WAN) is one that
connects multiple neighborhoods; the global internet is at the top of the
hierarchy.
-->

---

# OSI model

<Transform :scale="0.8">
<div grid="~ cols-2 gap-4">
<div id="osi">
Physical layer: It defines how data is transmitted in the physical medium
  in terms of its electrical and physical characteristics. This can either be by
  wire, fiber optic, or a wireless medium.

Data link layer: It defines how data is transmitted between two nodes connected
by a physical medium. This layer deals with prioritization between multiple
parties trying to access the wire simultaneously. Another important function of
this layer is to include some redundancy in the transmitted bits to minimize
errors during transmission. This is referred to as coding.

Network layer: It defines how packets (made up of multiple units of data) are
transmitted between networks. Thus, this layer needs to define how to identify
hosts and networks uniquely.

</div>
<div id="osi">
Transport layer: It defines mechanisms to reliably deliver variable length
messages to hosts (in the same or different networks). This layer defines a
stream of packets that the receiver can then listen to.

Session layer: It defines how applications running on hosts should communicate.
This layer needs to differentiate between applications running on the same host
and deliver packets to them.

Presentation layer: It defines common formats for data representation so that
different applications can interlink seamlessly. In some cases, this layer also
takes care of security.

Application layer: It defines how user-centric applications should send and
receive data. An example is the web browser (a user-centric application) using
HTTP (an application layer protocol) to talk to a web server.

</div>
</div>

</Transform>

---

<div class="grid grid-cols-2 gap-4 pt-4 -mb-6">

```mermaid {scale: 0.9}
sequenceDiagram
    Alice->John: Hello John, how are you?
```

</div>

---

## preload: false

# Animations

Animations are powered by [@vueuse/motion](https://motion.vueuse.org/).

```html
<div
  v-motion
  :initial="{ x: -80 }"
  :enter="{ x: 0 }">
  Slidev
</div>
```

<div class="w-60 relative mt-6">
  <div class="relative w-40 h-40">
    <img
      v-motion
      :initial="{ x: 800, y: -100, scale: 1.5, rotate: -50 }"
      :enter="final"
      class="absolute top-0 left-0 right-0 bottom-0"
      src="https://sli.dev/logo-square.png"
    />
    <img
      v-motion
      :initial="{ y: 500, x: -100, scale: 2 }"
      :enter="final"
      class="absolute top-0 left-0 right-0 bottom-0"
      src="https://sli.dev/logo-circle.png"
    />
    <img
      v-motion
      :initial="{ x: 600, y: 400, scale: 2, rotate: 100 }"
      :enter="final"
      class="absolute top-0 left-0 right-0 bottom-0"
      src="https://sli.dev/logo-triangle.png"
    />
  </div>

<div
    class="text-5xl absolute top-14 left-40 text-[#2B90B6] -z-1"
    v-motion
    :initial="{ x: -80, opacity: 0}"
    :enter="{ x: 0, opacity: 1, transition: { delay: 2000, duration: 1000 } }">
    Slidev
  </div>
</div>

<!-- vue script setup scripts can be directly used in markdown, and will only affects current page -->
<script setup lang="ts">
const final = {
  x: 0,
  y: 0,
  rotate: 0,
  scale: 1,
  transition: {
    type: 'spring',
    damping: 10,
    stiffness: 20,
    mass: 2
  }
}
</script>

<div
  v-motion
  :initial="{ x:35, y: 40, opacity: 0}"
  :enter="{ y: 0, opacity: 1, transition: { delay: 3500 } }">

[Learn More](https://sli.dev/guide/animations.html#motion)

</div>

---

# LaTeX

LaTeX is supported out-of-box powered by [KaTeX](https://katex.org/).

<br>

Inline $\sqrt{3x-1}+(1+x)^2$

Block $$ \begin{array}{c}

\nabla \times \vec{\mathbf{B}} -\, \frac1c\,
\frac{\partial\vec{\mathbf{E}}}{\partial t} & = \frac{4\pi}{c}\vec{\mathbf{j}}
\nabla \cdot \vec{\mathbf{E}} & = 4 \pi \rho \\

\nabla \times \vec{\mathbf{E}}\, +\, \frac1c\,
\frac{\partial\vec{\mathbf{B}}}{\partial t} & = \vec{\mathbf{0}} \\

\nabla \cdot \vec{\mathbf{B}} & = 0

\end{array} $$

<br>

[Learn more](https://sli.dev/guide/syntax#latex)

---

# Diagrams

You can create diagrams / graphs from textual descriptions, directly in your
Markdown.

<div class="grid grid-cols-2 gap-4 pt-4 -mb-6">

```mermaid {scale: 0.9}
sequenceDiagram
    Alice->John: Hello John, how are you?
    Note over Alice,John: A typical interaction
```

```mermaid {theme: 'neutral', scale: 0.8}
graph TD
B[Text] --> C{Decision}
C -->|One| D[Result 1]
C -->|Two| E[Result 2]
```

</div>

[Learn More](https://sli.dev/guide/syntax.html#diagrams)

---

## layout: center class: text-center

# Learn More

[Documentations](https://sli.dev) /
[GitHub Repo](https://github.com/slidevjs/slidev)
